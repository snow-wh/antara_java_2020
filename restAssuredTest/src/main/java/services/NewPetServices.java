package services;

import io.restassured.http.ContentType;
import io.restassured.internal.ResponseParserRegistrar;
import io.restassured.response.Response;

import modules.newPet.NewPet;
import modules.updateImage.UpdateImage;

import java.io.File;


import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.requestSpecification;
import static org.hamcrest.Matchers.hasItems;

public class NewPetServices {

    private String uri = "https://petstore.swagger.io/v2/pet/";

    public NewPet createPet(NewPet pet){

        Response response = given()
                .contentType(ContentType.JSON)
                .body(pet)
                .post(uri);

        if (response.statusCode() == 200){
            return response.as(NewPet.class);
        }
        return null;

    }

    public UpdateImage updateImage(int id){
        Response response = given()
                .pathParam("Key", id)
                .header("Content-Type","multipart/form-data")
                .multiPart(new File("src\\main\\resources\\puppy.jpg"))
                .multiPart("additionalMetadata", "sweet puppy")
                .post("https://petstore.swagger.io/v2/pet/{Key}/uploadImage");

        if (response.statusCode() == 200){
            return response.as(UpdateImage.class);
        }
        return null;
    }

    public NewPet updatePet(NewPet pet){
        Response response = given()
                .contentType(ContentType.JSON)
                .body(pet)
                .put(uri);

        if (response.statusCode() == 200){

            return response.as(NewPet.class);
        }
        return null;

    }


    public NewPet getPet (String key){

        Response response = given()
                .contentType(ContentType.JSON)
                .get( uri + key);

        if(response.statusCode()== 200){
            return response.as(NewPet.class);
        }
        return null;

    }

}
