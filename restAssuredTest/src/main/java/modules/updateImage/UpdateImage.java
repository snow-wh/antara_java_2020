
package modules.updateImage;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateImage {

    @JsonProperty("code")
    private Long mCode;
    @JsonProperty("message")
    private String mMessage;
    @JsonProperty("type")
    private String mType;



}
