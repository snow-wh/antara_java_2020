
package modules.newPet;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NewPet {

    @JsonProperty("category")
    private Category mCategory;
    @JsonProperty("id")
    private String mId;
    @JsonProperty("name")
    private String mName;
    @JsonProperty("photoUrls")
    private List<String> mPhotoUrls;
    @JsonProperty("status")
    private String mStatus;
    @JsonProperty("tags")
    private List<Tag> mTags;




}
