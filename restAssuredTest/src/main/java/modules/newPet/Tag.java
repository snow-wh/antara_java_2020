
package modules.newPet;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Tag {

    @JsonProperty("id")
    private Long mId;
    @JsonProperty("name")
    private String mName;


}
