package pet;

import static org.hamcrest.Matchers.*;
import io.restassured.http.ContentType;
import modules.newPet.Category;
import modules.newPet.NewPet;
import modules.newPet.Tag;

import modules.updateImage.UpdateImage;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import services.NewPetServices;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.jupiter.api.Assertions.*;

public class PetTest {

   private List<String> photoURL = new ArrayList<>();
   private List<Tag> tags = new ArrayList<>();
   private NewPetServices petServices = new NewPetServices();

   private String PET_ID = "9222968140496836077";


    /**
     * Другой вариант формирования запрса
     * Как делать правильней, как у вас с файла
     * или сериализацией класса?
     * */
    @DisplayName("create new pet by api")
    @Test
    void createNewPet(){

        photoURL.add("https://im0-tub-ru.yandex.net/i?id=7e3ca0b7b5eeeac59071e1ef1340ecfa&n=13");
        tags.add(Tag.builder()
                .mId((long)0)
                .mName("white wool")
                .build());


        NewPet pet = NewPet.builder()
                .mId("0")
                .mCategory(Category.builder()
                        .mId((long)1)
                        .mName("puppy")
                        .build())
                .mName("iron man")
                .mPhotoUrls(photoURL)
                .mTags(tags)
                .mStatus("available")
                .build();

        NewPet createdPet = petServices.createPet(pet);
        System.out.println(createdPet.getMId());
        assertNotNull(createdPet);
        assertEquals(pet.getMName(), createdPet.getMName());
        assertNotNull(createdPet.getMId());

    }

    @DisplayName("upload image")
    @Test
    void uploadImages(){

        UpdateImage updateImage = petServices.updateImage(922296814);

        assertNotNull(updateImage);
        assertTrue(updateImage.getMMessage().contains("puppy.jpg"));

    }

    @DisplayName("update pet")
    @Test
    void updatePet(){

        String status = "";

        NewPet firstPet = petServices.getPet(PET_ID);

        switch (firstPet.getMStatus()){
            case "available": status ="pending";
                break;
            case "pending" : status ="sold";
                break;
            case "sold" : status ="available";
                break;
        }

        photoURL.add("https://im0-tub-ru.yandex.net/i?id=7e3ca0b7b5eeeac59071e1ef1340ecfa&n=13");
        tags.add(Tag.builder()
                .mId((long)0)
                .mName("white wool")
                .build());


        NewPet pet = NewPet.builder()
                .mId(PET_ID)
                .mCategory(Category.builder()
                        .mId((long)1)
                        .mName("puppy")
                        .build())
                .mName("iron man")
                .mPhotoUrls(photoURL)
                .mTags(tags)
                .mStatus(status)
                .build();

        NewPet updatePet = petServices.updatePet(pet);
        assertNotEquals(firstPet.getMStatus(),updatePet.getMStatus());

    }

    private static Stream<Arguments> allGetStatusProvider() {
        return Stream.of(
                Arguments.of("available"),
                Arguments.of("sold"),
                Arguments.of("pending")
        );
    }

    @DisplayName("get pet for true status")
    @ParameterizedTest
    @MethodSource("allGetStatusProvider")
    void getPetForStatusOnTrueValue(String status){

        given().contentType(ContentType.JSON)
                .when()
                .get("https://petstore.swagger.io/v2/pet/findByStatus?status="+status)
                .then()
                .log()
                .body()
                .assertThat()
                .body("status",hasItems(status))
                .statusCode( HttpStatus.SC_OK);

    }

    /**
     * по неверному запросу не вылазит нужная ошибка
     * считаю юагом помечено Disable
     */
    @Disabled
    @DisplayName("get pet for false status")
    @Test
    void getPetForStatusOnFalseValue(){

        given().contentType(ContentType.JSON)
                .when()
                .get("https://petstore.swagger.io/v2/pet/findByStatus?status=sdfdfsdf")
                .then()
                .log()
                .body()
                .assertThat()
                .body("status",hasItems("sdfdfsdf"))
                .statusCode( HttpStatus.SC_BAD_REQUEST);

    }

    private static Stream<Arguments> wrongUrlGetProvider() {
        return Stream.of(
                Arguments.of("https://petstore.swagger.io/v2/pet/qwe"),
                Arguments.of("https://petstore.swagger.io/v2/pet/922337203685477580800"),
                Arguments.of("https://petstore.swagger.io/v2/pet/0x100"),
                Arguments.of("https://petstore.swagger.io/v2/pet/10.12"),
                Arguments.of("https://petstore.swagger.io/v2/pet/%$#@!@")
        );
    }

    @DisplayName("wrong url query")
    @ParameterizedTest
    @MethodSource("wrongUrlGetProvider")
    void getPetOnWrongQuery(String url){
        given().contentType(ContentType.JSON)
                .when()
                .get(url)
                .then()
                .log()
                .body()
                .assertThat()
                .body("code", oneOf(400, 404));

    }
    @DisplayName("get pet for id")
    @Test
    void getPetForId(){
        given().contentType(ContentType.JSON)
                .when()
                .get("https://petstore.swagger.io/v2/pet/9222968140496836077")
                .then()
                .log()
                .body()
                .assertThat()
                .statusCode(HttpStatus.SC_OK);

    }
    @DisplayName("delete true pet")
    @Test
    void deleteTruePet(){
        given()
                .when()
                .delete("https://petstore.swagger.io/v2/pet/" + "9222968140496836115")
                .then()
                .assertThat()
                .statusCode(200);
    }
    @DisplayName("delete false pet")
    @Test
    void deleteFalsePet(){
        given()
                .when()
                .delete("https://petstore.swagger.io/v2/pet/" + "9222968140496836115")
                .then()
                .assertThat()
                .statusCode(404);
    }

}
