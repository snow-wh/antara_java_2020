package animals;

import sizeEnum.CageSize;

public class Elefant extends Herbivorous {

    private CageSize size = CageSize.OVER_BIG_ANIMAL;

    public Elefant(int weight, String name) {
        super(weight, name);
    }

    @Override
    public CageSize getCageSize() {
        return size;
    }

    @Override
    protected String voice() {
        return "туруруруруру";
    }

}
