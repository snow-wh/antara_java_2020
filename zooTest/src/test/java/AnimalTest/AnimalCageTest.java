package AnimalTest;

import animals.Carnivorous;
import animals.Duck;
import animals.Herbivorous;
import animals.Lion;
import cage.CarnivorousCage;
import cage.HerbivorousCage;
import exception.WrongCageContainException;
import exception.WrongCageSizeException;
import exception.WrongFoodException;
import exception.WrongKillAnimalException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@DisplayName("Тестирование клеток")
public class AnimalCageTest {

    private PrintStream stream;
    private ArgumentCaptor<String> captor;
    private ByteArrayOutputStream output;

    private String name = "lion";

    private String LIVING_IN = " живет теперь тут";
    private String LIVING_OUT = " выселен из клетки";
    private String SAME_ANIMAL = "Это животное уже в клетке";
    private String KILL_ANIMAL = "Это животное мертво";
    private String FEED_ALL = "'y не хватило 3373 кКалорий, добавьте порцию";

    @BeforeEach
    void setUp() {

        stream = mock(PrintStream.class);
        captor = ArgumentCaptor.forClass(String.class);
    }

    @DisplayName("Поселение животного в подходящую кетку")
    @Test
    void shouldAnimalMoveInCage() throws WrongCageSizeException {
        CarnivorousCage<Carnivorous> cage = new CarnivorousCage<>(20,20);
        System.setOut(stream);

        cage.moveIn(new Lion(name,250));

        verify(stream).println(captor.capture());
        assertEquals(captor.getValue(), name + LIVING_IN);

    }

    @DisplayName("Поселение животного в маленькую кетку")
    @Test
    void shouldAnimalMoveInSmallCage(){
        CarnivorousCage<Carnivorous> cage = new CarnivorousCage<>(2,3);
        assertThrows(
                WrongCageSizeException.class,
                () -> cage.moveIn(new Lion(name,250)));

    }
    @DisplayName("Поселение того же животного в кетку")
    @Test
    void shouldSameAnimalMoveInCage() throws WrongCageSizeException {
        CarnivorousCage<Carnivorous> cage = new CarnivorousCage<>(2,5);
        cage.moveIn(new Lion(name,250));
        System.setOut(stream);

        cage.moveIn(new Lion(name,250));

        verify(stream).println(captor.capture());
        assertEquals(captor.getValue(), SAME_ANIMAL );

    }

    @DisplayName("Поселение мертвого животного")
    @Test
    void shouldKillAnimalMoveInCage() throws WrongKillAnimalException, WrongFoodException {
        HerbivorousCage<Herbivorous> cage = new HerbivorousCage<>(2,5);

        Duck duck = new Duck(4, "duck");
        Lion lion = new Lion("lion", 250);

        lion.eat(duck.kill());
        System.setOut(stream);

        cage.moveIn(duck);

        verify(stream).println(captor.capture());
        assertEquals(captor.getValue(), KILL_ANIMAL );

    }

    @DisplayName("Выселение животного из клетки")
    @Test
    void shouldAnimalMoveOutCage() throws WrongCageSizeException, WrongCageContainException {
        CarnivorousCage<Carnivorous> cage = new CarnivorousCage<>(20,20);
        cage.moveIn(new Lion(name,250));
        cage.moveIn(new Lion(name + "1",250));
        System.setOut(stream);

        cage.moveOut(new Lion(name,250));

        verify(stream).println(captor.capture());
        assertEquals(captor.getValue(), name + LIVING_OUT);

    }

    @DisplayName("Выселение несуществующего животного из клетки")
    @Test
    void shouldNoneAnimalMoveOutCage(){
        CarnivorousCage<Carnivorous> cage = new CarnivorousCage<>(2,3);
        assertThrows(
                WrongCageContainException.class,
                () -> cage.moveOut(new Lion(name,250)));

    }

    @DisplayName("Кормление пустой кетки")
    @Test
    void shouldEatAnimalEmptyCage(){
        CarnivorousCage<Carnivorous> cage = new CarnivorousCage<>(2,3);
        assertThrows(
                NullPointerException.class,
                cage::feedAll);

    }


    @DisplayName("Кормление всей кетки")
    @Test
    void shouldEatAnimalAllCage() throws WrongCageSizeException {

        CarnivorousCage<Carnivorous> cage = new CarnivorousCage<>(20,20);

        output = new ByteArrayOutputStream();

        cage.moveIn(new Lion(name, 250));
        cage.moveIn(new Lion(name+"1", 250));

        System.setOut(new PrintStream(output));
        cage.feedAll();

        assertEquals(output.toString(),name + "1"+FEED_ALL + "\r\n" + name + FEED_ALL+"\r\n");

    }

}
