package AnimalTest;

import animals.Duck;
import animals.Lion;
import exception.WrongFoodException;
import exception.WrongKillAnimalException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.io.PrintStream;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

@DisplayName("Смерть Травоядных животных")
public class HerbivorousAnimalTest {

    private Duck duck;

    private PrintStream stream;
    private ArgumentCaptor<String> captor;

    @BeforeEach
    void setUp() {
        duck = new Duck(4, "duck");
        stream = mock(PrintStream.class);
        captor = ArgumentCaptor.forClass(String.class);
    }

    @DisplayName("Подтверждение смерти животного")
    @Test
    void shouldSayAnimalKll() throws WrongKillAnimalException, WrongFoodException {

        Lion lion = new Lion( "lion", 250);

        lion.eat(duck.kill());

        assertTrue(duck.getKill());
    }

}
