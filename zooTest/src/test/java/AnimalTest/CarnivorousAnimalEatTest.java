package AnimalTest;

import animals.Carnivorous;
import animals.Duck;
import animals.Lion;
import cage.CarnivorousCage;
import exception.WrongFoodException;
import exception.WrongKillAnimalException;
import food.Chicken;
import food.Meat;
import food.Seed;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static org.mockito.Mockito.*;

import java.io.*;


import static org.junit.jupiter.api.Assertions.*;


@DisplayName("Кормление Хищных Животных")
public class CarnivorousAnimalEatTest {

    private Lion lion;
    private String name = "lion";
    private int weight = 280;

    private PrintStream stream;
    private ArgumentCaptor<String> captor;
    private ByteArrayOutputStream output;

    private String FULL_ANIMAL = " наелся, повторите через 5 часов";
    private String A_LOT_FOOD = "'у многовато, уменьшите порцию на  2277 кКалорий";
    private String LESS_FOOD = "'y не хватило 1623 кКалорий, добавьте порцию";
    private String WAIT_TIME = "'a пока нельзя кормить";


    @BeforeEach
    void setUp() {
        lion = new Lion(name, weight);
        stream = mock(PrintStream.class);
        captor = ArgumentCaptor.forClass(String.class);
    }

    @DisplayName("Кормление льва нужной едой в нормальных количествах")
    @Test
    void shouldEatHisFood() {

        System.setOut(stream);

        try {
            lion.eat(new Chicken(10));
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }

        verify(stream).println(captor.capture());
        assertEquals(captor.getValue(), name + FULL_ANIMAL);

    }

    @DisplayName("Кормление льва нужной едой, но в больших количествах")
    @Test
    void shouldSayALotFood() {

        System.setOut(stream);

        try {
            lion.eat(new Meat(5));
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }

        verify(stream).println(captor.capture());
        assertEquals(captor.getValue(), name + A_LOT_FOOD);

    }

    @DisplayName("Кормление льва нужной едой, но в меньших количествах")
    @Test
    void shouldSayLessFood() throws WrongFoodException {

        System.setOut(stream);

        lion.eat(new Meat(2));

        verify(stream).println(captor.capture());
        assertEquals(captor.getValue(), name + LESS_FOOD);

    }

    @DisplayName("Кормление льва неподходящей едой")
    @Test
    void shouldSayNotMyFood() {

        assertThrows(
                WrongFoodException.class,
                () -> lion.eat(new Seed(1)));

    }

    @DisplayName("Разовая норма кормления животного")
    @Test
    void timerTestAnimalEat() throws WrongFoodException, InterruptedException {

        output = new ByteArrayOutputStream();

        System.setOut(new PrintStream(output));
        lion.eat(new Chicken(10));
        assertEquals(output.toString(), name + FULL_ANIMAL + "\r\n");
        output.reset();

        Thread.sleep(60000);

        System.setOut(new PrintStream(output));
        lion.eat(new Meat(2));
        assertEquals(output.toString(), name+WAIT_TIME + "\r\n");
        output.reset();

        Thread.sleep(60000);

        System.setOut(new PrintStream(output));
        lion.eat(new Meat(2));
        assertEquals(output.toString(), name + LESS_FOOD+"\r\n");
        output.reset();
    }

    @DisplayName("Кормление уже съеденой уткой")
    @Test
    void shouldSayKillAnimal() throws WrongKillAnimalException, WrongFoodException {

        Duck duck = new Duck(3, "duck");

        lion.eat(duck.kill());

        assertThrows(
                WrongKillAnimalException.class,
                () -> lion.eat(duck.kill()));

    }

}
