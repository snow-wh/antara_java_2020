package cage;


import exception.WrongCageSizeException;
import exception.WrongCageContainException;

import animals.Carnivorous;

import food.Chicken;

import food.Food;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CarnivorousCage <T extends Carnivorous>  extends Cage{

    private static Logger logger = LoggerFactory.getLogger(CarnivorousCage.class);

    public CarnivorousCage(int length, int width) {
        super(length, width);
    }


    public void moveIn(T animal) throws WrongCageSizeException {

        if (!animal.getKill()) {
            if (containsAnimal(animal)) {
                    in(animal);
                    logger.info(animal.name() + " живет теперь тут");
                    System.out.println(animal.name() + " живет теперь тут");

            } else {
                logger.warn("Это животное уже в клетке");
                System.out.println("Это животное уже в клетке");
            }
        } else {
            logger.error("Это животное мертво");
            System.out.println("Это животное мертво");
        }
    }


    public void moveOut(T animal) throws WrongCageContainException {


            out(animal);
            logger.info(animal.name() + " выселен из клетки");
            System.out.println(animal.name() + " выселен из клетки");


    }

    public void feedAll(){

        feedAnimal(new Chicken(1));
    }

}
