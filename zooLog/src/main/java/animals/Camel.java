package animals;

import sizeEnum.CageSize;

public class Camel extends Herbivorous {

    private CageSize size = CageSize.BIG_ANIMAL;

    public Camel(int weight, String name) {
        super(weight, name);
    }

    @Override
    public CageSize getCageSize() {
        return size;
    }

    @Override
    public String voice() {
        return "мумуму";
    }

}
