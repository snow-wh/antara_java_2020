package animals;

import sizeEnum.CageSize;

public class Lion extends Carnivorous {

    private CageSize size = CageSize.MID_ANIMAL;

    public Lion(String name, int weight) {
        super(weight,name);
    }

    @Override
    public CageSize getCageSize() {
        return size;
    }

    @Override
    public String voice() {
        return "rrrrr";
    }

}
