package food;

public abstract class Food {

    public abstract int caloricity();

    public abstract String origin();

}
