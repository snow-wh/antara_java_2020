package cage;

import animals.Animal;
import animals.Herbivorous;

import java.util.HashMap;
import java.util.Map;

public class HerbivorousCage extends Cage {

    private final int BIG_ANIMAL = 2000;
    private final int MID_ANIMAL = 800;
    private final int SMALL_ANIMAL = 100;
    private final int OVER_SMALL_ANIMAL = 5;

    private Map<String, Animal> neighbours = new HashMap<>();

    public HerbivorousCage(int length, int width) {
        super(length, width);
    }


    public void moveIn(Animal animal) {

        if (animal instanceof Herbivorous) {

            if (death(animal)) {
                if (presence(animal, neighbours)) {
                    try {
                        if (in(animal, neighbours, BIG_ANIMAL, 200, MID_ANIMAL, 100, SMALL_ANIMAL, 10, OVER_SMALL_ANIMAL, 3) != null) {
                            neighbours = in(animal, neighbours, BIG_ANIMAL, 200, MID_ANIMAL, 100, SMALL_ANIMAL, 10, OVER_SMALL_ANIMAL, 3);
                            System.out.println(animal.name() + " теперь живет в этой клетке");
                        } else {
                            System.out.println("Клетка для " + animal.name() + "'а маловата");
                        }
                    } catch (Exception e) {
                        System.out.println("Неверные переданные данные");
                    }
                } else {
                    System.out.println("Это животное уже в клетке");
                }
            } else {
                System.out.println("Это животное мертво");
            }
        } else {
            System.out.println("Нельзя селить разные виды животных в одну клетку");
        }

    }

    public void moveOut(Animal animal) {

        if(out(animal,neighbours)!=null){
            neighbours=out(animal,neighbours);
            System.out.println(animal.name() + " выселен из клетки");
        }  else {
            System.out.println("Поищите в другой клетке");
        }

    }

}
