package cage;


import animals.Animal;

import java.util.Map;

public class Cage {

    private int length;
    private int width;


    public Cage(int length, int width) {
        this.length = length;
        this.width = width;
    }

    /**
     * Cage.in
     * if input mass != 8 standard => Exception
     */
    protected Map in(Animal animal, Map<String, Animal> neighbours, int... sizes) throws Exception {

        if (sizes.length!=8)
            throw new Exception();

        int allWeight = 0;
        int count = 1;
        if (!neighbours.isEmpty()) {
            for (Animal animals : neighbours.values()) {
                allWeight += animals.weight();
                count += 1;
            }
        }
        if ((allWeight + animal.weight()) >= (count * sizes[0])) {
            if (length * width >= count * sizes[1]) {
                neighbours.put(animal.name(), animal);
                return neighbours;
            } else {
                return null;
            }
        } else if (allWeight + animal.weight() >= count * sizes[2] && count * sizes[0] < allWeight + animal.weight()) {
            if (length * width >= count * sizes[3]) {
                neighbours.put(animal.name(), animal);
                return neighbours;
            } else {
                return null;
            }
        } else if (allWeight + animal.weight() >= count * sizes[4] && count * sizes[2] < allWeight + animal.weight()) {
            if (length * width >= count * sizes[5]) {
                neighbours.put(animal.name(), animal);
                return neighbours;
            } else {
                return null;
            }
        } else if (allWeight + animal.weight() >= count * sizes[6] && count * sizes[4] < allWeight + animal.weight()) {
            if (length * width >= count * sizes[7]) {
                neighbours.put(animal.name(), animal);
                return neighbours;
            } else {
                return null;
            }
        }

        return null;
    }

    protected boolean death(Animal animal) {
        return animal.weight() != 0;
    }

    protected boolean presence(Animal animal, Map<String, Animal> neighbours) {
        return !neighbours.containsKey(animal.name());
    }

    protected Map out(Animal animal, Map<String, Animal> neighbours) {

        if (neighbours.containsKey(animal.name())) {
            neighbours.remove(animal.name());
            return neighbours;
        } else {
            return null;
        }
    }

}
