package animals;

public class Lion extends Carnivorous {

    private String name;

    public Lion(String name, int weight) {
        super(weight);
        this.name = name;
    }

    @Override
    public String voice() {
        return "rrrrr";
    }

    @Override
    public String name() {
        return name;
    }

}
