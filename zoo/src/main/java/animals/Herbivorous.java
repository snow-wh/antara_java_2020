package animals;

import food.Food;
import timer.Timer;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Herbivorous extends Animal implements Timer {
    private int weight;
    private int counterCalories;
    private int timeToEat;

    protected Herbivorous(int weight) {
        this.weight = weight;
    }

    @Override
    public int weight() {
        if (getKill())
            weight = 0;
        return weight;
    }

    @Override
    protected int amountOfFood(Food food) {

        int dayNorm = 25 * weight;
        int returnCount;


        if (food.origin().equals("herbivorous")) {
            if (timeToEat == 0 || timeToEat <= timerRun(0)) {
                if (counterCalories + food.caloricity() >= dayNorm / 3 - 250 && counterCalories + food.caloricity() < dayNorm / 3 + 250) {
                    returnCount = 1;
                    timeToEat = timerRun(300);
                    counterCalories = 0;
                } else if (counterCalories + food.caloricity() <= dayNorm / 3) {
                    counterCalories += food.caloricity();
                    returnCount = dayNorm / 3 - counterCalories;
                } else {
                    returnCount = dayNorm / 3 - (counterCalories + food.caloricity());
                }
            } else {
                returnCount = 0;
            }
        } else {
            returnCount = -1;
        }


        return returnCount;
    }

    @Override
    public int timerRun(int waitTime) {

        Date timeNow = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH,mm");
        String s = sdf.format(timeNow);
        String[] str = s.split(",");
        return (int) Math.round((Integer.parseInt(str[0]) * 60 + Integer.parseInt(str[1])) + waitTime * 0.0056);
    }

}
