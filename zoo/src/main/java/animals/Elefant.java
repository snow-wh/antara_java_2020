package animals;


public class Elefant extends Herbivorous {

    private String name;

    public Elefant(int weight, String name) {
        super(weight);
        this.name = name;
    }

    @Override
    protected String voice() {
        return "туруруруруру";
    }

    @Override
    public String name() {
        return name;
    }

}
