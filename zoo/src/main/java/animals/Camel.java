package animals;

public class Camel extends Herbivorous {

    private String name;

    public Camel(int weight, String name) {
        super(weight);
        this.name = name;
    }

    @Override
    public String voice() {
        return "мумуму";
    }

    @Override
    public String name() {
        return name;
    }

}
