package animals;

import food.AnimalOrigin;
import food.Food;


public class Duck extends Herbivorous {

    private String name;
    private int weight;

    public Duck(int weight, String name) {
        super(weight);
        this.weight = weight;
        this.name = name;
    }

    @Override
    protected String voice() {
        if (getKill() == false)
            return "крякря";
        else
            return "убили";
    }

    @Override
    public String name() {
        return name;
    }


    public Food kill() throws Exception {
        if (getKill() == false) {
            setKill(true);
            return new AnimalOrigin(1, 308 * weight);
        } else {
            throw new Exception();
        }
    }

}
