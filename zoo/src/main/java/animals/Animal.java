package animals;

import food.Food;

public abstract class Animal {

    private boolean kill = false;

    protected abstract String voice();

    public abstract String name();

    public abstract int weight();

    protected abstract int amountOfFood(Food food);

    protected boolean getKill(){
        return kill;
    }
    protected void setKill(boolean kill){
        this.kill=kill;
    }

    public void eat(Food food) {
        int returnAmount = amountOfFood(food);

        switch (returnAmount) {
            case -1:
                System.out.println("Принесите " + name() + "'y мяса");
                break;
            case 0:
                System.out.println(name() + "'a пока нельзя кормить");
                break;
            case 1:
                System.out.println(name() + " наелся, повторите через 5 часов");
                break;
            default:
                if (returnAmount > 0)
                    System.out.println(name() + "'y не хватило " + returnAmount + " кКалорий, добавьте порцию");
                else
                    System.out.println(name() + "'у многовато, уменьшите порцию на  " + Math.abs(returnAmount) + " кКалорий");

        }
    }

}

