package ZooSerialiseTest;

import animals.Camel;
import animals.Elefant;
import animals.Lion;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import exception.WrongCageContainException;
import org.junit.jupiter.api.Test;
import zoo.Zoo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ZooTest {


    @Test
    void serialize() {

        Zoo zoo = new Zoo();
       zoo.createCage(10,5,new Lion("lion",250),new Lion("lion1",250));
       zoo.createCage(50,50,new Elefant(1500,"elefant"),new Camel(600,"camel"));

        try {
            XmlMapper xmlMapper = new XmlMapper();


            String xmlString = xmlMapper
                    .writeValueAsString(zoo);

            File xmlOutput = new File("serialized.xml");
            FileWriter fileWriter = new FileWriter(xmlOutput);
            fileWriter.write(xmlString);
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Test
    void deSerialize() throws IOException, WrongCageContainException {

        XmlMapper xmlMapper = new XmlMapper();

        String readContent = new String(Files.readAllBytes(Paths.get("serialized.xml")));

        Zoo zoo = xmlMapper.readValue(readContent, Zoo.class);

        zoo.getCages(1).feedAll();
        zoo.getCages(2).feedAll();


    }
}
