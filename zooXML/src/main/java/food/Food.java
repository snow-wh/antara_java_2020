package food;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class Food {

    @JsonIgnore
    public abstract int caloricity();


}
