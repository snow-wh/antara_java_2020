package zoo;

import animals.Carnivorous;
import animals.Herbivorous;
import cage.Cage;
import cage.CarnivorousCage;
import cage.HerbivorousCage;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import exception.WrongCageSizeException;

import java.util.ArrayList;
import java.util.List;


@JsonRootName("Zoo")
public class Zoo {


    @JacksonXmlElementWrapper (localName = "cages")
    @JsonProperty("Cage")
    private List<Cage> cages = new ArrayList<>();

    private CarnivorousCage<Carnivorous> carnivorousCage;
    private HerbivorousCage<Herbivorous> herbivorousCage;

    public Zoo(){}

    public void createCage(int length, int width, Carnivorous ... animal){

        carnivorousCage = new CarnivorousCage<>(length,width);
        try {
            carnivorousCage.moveIn(animal);
        } catch (WrongCageSizeException e) {
            e.printStackTrace();
        }
        cages.add(carnivorousCage);

    }
    public void createCage(int length, int width,Herbivorous ... animal){

        herbivorousCage = new HerbivorousCage<>(length,width);
        try {
            herbivorousCage.moveIn(animal);
        } catch (WrongCageSizeException e) {
            e.printStackTrace();
        }
        cages.add(herbivorousCage);

    }

    public Cage getCages(int idNumber){
        return cages.get(idNumber-1);
    }


}
