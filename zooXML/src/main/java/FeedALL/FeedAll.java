package FeedALL;

import food.Food;

public interface FeedAll {

    void feedAll(Food food);
}
