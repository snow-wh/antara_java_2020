package animals;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import exception.WrongKillAnimalException;
import food.AnimalOrigin;
import food.Food;
import sizeEnum.CageSize;

@JsonTypeName("Duck")
public class Duck extends Herbivorous {

    @JacksonXmlProperty(localName = "Duck_Cage_Size")
    private final CageSize size = CageSize.SMALL_ANIMAL;
    @JsonIgnore
    private int weight;

    public Duck(int weight,String name ) {
        setName(name);
        setWeight(weight);
    }
    public Duck(){}

    @JsonIgnore
    @Override
    public CageSize getCageSize() {
        return size;
    }

    @Override
    protected String voice() {
        if (!getKill())
            return "крякря";
        else
            return "убили";
    }


    public Food kill() throws WrongKillAnimalException {
        if (!getKill()) {
            setKill(true);
            return new AnimalOrigin(1, 308 * weight);
        } else {
            throw new WrongKillAnimalException();
        }
    }

}
