package animals;

import com.fasterxml.jackson.annotation.*;
import exception.WrongFoodException;
import food.Food;
import food.HerbivorousOrigin;
import timer.Timer;

import java.text.SimpleDateFormat;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY,property = "@type")
@JsonSubTypes({
        @JsonSubTypes.Type(name = "Camel", value = Camel.class),
        @JsonSubTypes.Type(name = "Duck", value = Duck.class),
        @JsonSubTypes.Type(name = "Elefant", value = Elefant.class)
})
public abstract class Herbivorous extends Animal implements Timer {

    @JsonProperty
    private int weight;
    @JsonIgnore
    private int counterCalories;
    @JsonProperty
    private int timeToEat;

    protected Herbivorous() { }
    protected Herbivorous(int weight, String name) {
        setName(name);
        this.weight = weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }



    public int getWeight() {
        return weight;
    }



    @Override
    protected int amountOfFood(Food food) throws WrongFoodException {

        int dayNorm = 25 * weight;
        int returnCount;


        if (food instanceof HerbivorousOrigin) {
            if (timeToEat == 0 || timeToEat <= timerRun(0)) {
                if (counterCalories + food.caloricity() >= dayNorm / 3 - 250 && counterCalories + food.caloricity() < dayNorm / 3 + 250) {
                    returnCount = 1;
                    timeToEat = timerRun(300);
                    counterCalories = 0;
                } else if (counterCalories + food.caloricity() <= dayNorm / 3) {
                    counterCalories += food.caloricity();
                    returnCount = dayNorm / 3 - counterCalories;
                } else {
                    returnCount = dayNorm / 3 - (counterCalories + food.caloricity());
                }
            } else {
                returnCount = 0;
            }
        } else {
            throw new WrongFoodException();
        }


        return returnCount;
    }

    @Override
    public int timerRun(int waitTime) {

        Date timeNow = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH,mm");
        String s = sdf.format(timeNow);
        String[] str = s.split(",");
        return (int) Math.round((Integer.parseInt(str[0]) * 60 + Integer.parseInt(str[1])) + waitTime * 0.0056);
    }

}
