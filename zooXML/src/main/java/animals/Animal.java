package animals;

import FeedALL.FeedAll;
import cage.CarnivorousCage;
import cage.HerbivorousCage;
import com.fasterxml.jackson.annotation.*;
import exception.WrongFoodException;
import food.Food;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sizeEnum.CageSize;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY,property = "@type")
@JsonSubTypes({
        @JsonSubTypes.Type(name = "Carnivorous", value = Carnivorous.class),
        @JsonSubTypes.Type(name = "Herbivorous", value = Herbivorous.class),

})
public abstract class Animal implements FeedAll {



    @JsonIgnore
    private static Logger logger = LoggerFactory.getLogger(Animal.class);

    @JsonProperty()
    protected String name;
    @JsonProperty()
    private boolean kill = false;



    public abstract CageSize getCageSize();

    protected abstract String voice();

    protected abstract int amountOfFood(Food food) throws WrongFoodException;

    public boolean getKill(){
        return kill;
    }

    protected void setKill(boolean kill){
        this.kill=kill;
    }

    public void eat(Food food) throws WrongFoodException {
        int returnAmount = 0;
            returnAmount = amountOfFood(food);
            switch (returnAmount) {
                case 0:
                    logger.info(getName() + "'a пока нельзя кормить");
                    System.out.println(getName() + "'a пока нельзя кормить");
                    break;
                case 1:
                    logger.info(getName() + " наелся, повторите через 5 часов");
                    System.out.println(getName() + " наелся, повторите через 5 часов");
                    break;
                default:
                    if (returnAmount > 0){
                        logger.info(getName() + "'y не хватило " + returnAmount + " кКалорий, добавьте порцию");
                        System.out.println(getName() + "'y не хватило " + returnAmount + " кКалорий, добавьте порцию");
                    }
                    else{
                        logger.info(getName() + "'у многовато, уменьшите порцию на  " + Math.abs(returnAmount) + " кКалорий");
                        System.out.println(getName() + "'у многовато, уменьшите порцию на  " + Math.abs(returnAmount) + " кКалорий");
                    }

            }
    }
    public String getName() {
        return name;
    }

    protected void setName(String name){
        this.name = name;
    }

    @Override
    public void feedAll(Food food){
        try {
            eat(food);
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }
    }

}
