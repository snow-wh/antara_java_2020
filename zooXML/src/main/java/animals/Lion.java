package animals;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import sizeEnum.CageSize;

@JsonTypeName("Lion")
public class Lion extends Carnivorous {

    @JacksonXmlProperty(localName = "Lion_Cage_Size")
    private CageSize size = CageSize.MID_ANIMAL;

    public Lion(String name, int weight) {
        setName(name);
        setWeight(weight);
    }
    public Lion(){}

    @JsonIgnore
    @Override
    public CageSize getCageSize() {
        return size;
    }

    @Override
    public String voice() {
        return "rrrrr";
    }

}
