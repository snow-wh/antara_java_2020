package animals;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import sizeEnum.CageSize;

@JsonTypeName("Elefant")
public class Elefant extends Herbivorous {

    @JacksonXmlProperty(localName = "Elefant_Cage_Size")
    private final CageSize size = CageSize.OVER_BIG_ANIMAL;

    public Elefant(int weight,String name) {
        setName(name);
        setWeight(weight);
    }
    public Elefant(){}

    @JsonIgnore
    @Override
    public CageSize getCageSize() {
        return size;
    }

    @Override
    protected String voice() {
        return "туруруруруру";
    }

}
