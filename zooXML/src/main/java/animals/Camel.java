package animals;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import sizeEnum.CageSize;

@JsonTypeName("Camel")
public class Camel extends Herbivorous {


    @JacksonXmlProperty(localName = "Camel_Cage_Size")
    private final CageSize size = CageSize.BIG_ANIMAL;

    public Camel(int weight,String name) {
        setName(name);
        setWeight(weight);
    }
    public Camel(){}

    @JsonIgnore
    @Override
    public CageSize getCageSize() {
        return size;
    }

    @Override
    public String voice() {
        return "мумуму";
    }

}
