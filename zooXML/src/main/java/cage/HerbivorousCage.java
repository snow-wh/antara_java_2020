package cage;

import animals.Animal;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import exception.WrongCageSizeException;
import exception.WrongCageContainException;

import animals.Herbivorous;

import food.Seed;

import java.util.Arrays;

@JsonTypeName("HerbivorousCage")
public class HerbivorousCage<T extends Herbivorous> extends Cage  {

    public HerbivorousCage(int length, int width) {
        setLength(length);
        setWidth(width);
    }

    public HerbivorousCage(){}

    @SafeVarargs
    public final void moveIn(T... animal) throws WrongCageSizeException {
        in(Arrays.asList(animal));
    }

    @Override
    public void feedAll() {
        feedAnimal(new Seed(1));
   }
}
