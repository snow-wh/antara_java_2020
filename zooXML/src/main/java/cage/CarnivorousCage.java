package cage;


import animals.Animal;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import exception.WrongCageSizeException;
import exception.WrongCageContainException;

import animals.Carnivorous;

import food.Chicken;


import java.util.Arrays;


@JsonTypeName("CarnivorousCage")
public class CarnivorousCage<T extends Carnivorous> extends Cage {

    public CarnivorousCage(int length, int width) {
        setLength(length);
        setWidth(width);
    }

    public CarnivorousCage(){}


    @Override
    public void feedAll() {
        feedAnimal(new Chicken(1));
    }


    @SafeVarargs
    public final void moveIn(T... animal) throws WrongCageSizeException {
        in(Arrays.asList(animal));
    }



}
