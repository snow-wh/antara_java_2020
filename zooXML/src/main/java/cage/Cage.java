package cage;

import FeedALL.FeedAll;
import animals.Animal;
import com.fasterxml.jackson.annotation.*;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import exception.WrongCageSizeException;
import exception.WrongCageContainException;
import food.Food;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY,property = "@type")
@JsonSubTypes({
         @JsonSubTypes.Type(name = "CarnivorousCage", value = CarnivorousCage.class),
        @JsonSubTypes.Type(name = "HerbivorousCage", value = HerbivorousCage.class)
})


public abstract class Cage {

    @JsonProperty
    private int length;
    @JsonProperty
    private int width;

    @JsonIgnore
    private static Logger logger = LoggerFactory.getLogger(CarnivorousCage.class);

    @JacksonXmlElementWrapper()
    @JacksonXmlProperty()
    private Map<String, Animal> neighbours = new HashMap<>();

    protected void setLength(int length){
        this.length=length;
    }
    protected void setWidth(int width){
        this.width=width;
    }


    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }

    protected void in(List<Animal> animal) throws WrongCageSizeException {

        int allSize = 0;
        int animalSize = 0;

        if (!getKill(animal)) {
            if (!containsAnimal(animal)) {

                for (Animal animals : neighbours.values()) {
                    allSize += animals.getCageSize().getSize();
                }
                for (Animal animals : animal) {
                    animalSize += animals.getCageSize().getSize();
                }
                if (allSize + animalSize <= (length * width)) {
                    for (Animal animals : animal) {
                        neighbours.put(animals.getName(), animals);
                    }
                } else {
                    throw new WrongCageSizeException();
                }
            } else {
                logger.warn("Это животное уже в клетке");
                System.out.println("Это животное уже в клетке");
            }
        } else {
            logger.error("Это животное мертво");
            System.out.println("Это животное мертво");
        }

    }

    protected void feedAnimal(Food food) {

        if (!neighbours.isEmpty()) {
            for (Animal animals : neighbours.values()) {
                ((FeedAll) animals).feedAll(food);
            }
        } else {
            throw new NullPointerException();
        }
    }

    private boolean containsAnimal(List<Animal> animal) {

        boolean temp = false;

        for (Animal animals : animal) {
            temp = neighbours.containsKey(animals.getName());
        }

        return temp;
    }
    private boolean containsAnimal(String name) {

        return !neighbours.containsKey(name);
    }

    private boolean getKill(List<Animal> animal) {

        return animal.stream().allMatch(Animal::getKill);
    }

    protected void out(String name) throws WrongCageContainException {

        if (!containsAnimal(name)) {
                neighbours.remove(name);
                logger.info(name + " выселен из клетки");
                System.out.println(name + " выселен из клетки");
        } else {
            throw new WrongCageContainException();
        }
    }

    public void moveOut(String name) throws WrongCageContainException {
        out(name);
    }

    public void moveOut(Animal animal) throws WrongCageContainException {
        out(animal.getName());
    }

    public abstract void feedAll();



}
