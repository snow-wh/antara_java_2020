SELECT pc.model, pc.speed, pc.hd
FROM PC pc
WHERE pc.PRICE < '500'

-----------------1---------------

SELECT DISTINCT pr.MAKER 
FROM PRODUCT pr
WHERE pr.TYPE = 'Printer'

-----------------2---------------

SELECT lt.MODEL, lt.RAM, lt.SCREEN 
FROM LAPTOP lt 
WHERE lt.PRICE > '1000'

-----------------3---------------

SELECT *
FROM PRINTER pr
WHERE pr.TYPE = 'Jet'
ORDER BY code

-----------------4---------------

SELECT pc.model, pc.speed, pc.hd 
FROM PC pc WHERE (pc.cd = '12x' OR pc.cd = '24x') 
AND pc.price < 600

-----------------5---------------

SELECT DISTINCT pr.maker, lt.speed 
FROM Product pr
JOIN Laptop lt ON pr.model = lt.model
WHERE lt.hd >= '10'

-----------------6---------------

SELECT model, price 
FROM PC 
WHERE model = (SELECT model FROM Product WHERE maker = 'B' AND type = 'PC')
UNION
SELECT model, price 
FROM Laptop 
WHERE model = (SELECT model FROM Product WHERE maker = 'B' AND type = 'Laptop')
UNION
SELECT model, price 
FROM Printer 
WHERE model = (SELECT model FROM Product WHERE maker = 'B' AND type = 'Printer')

-----------------7---------------

SELECT maker
FROM product
WHERE type='PC' 
EXCEPT
SELECT maker
FROM product
WHERE type='Laptop'

-----------------8---------------

SELECT DISTINCT p.maker 
FROM Product p 
JOIN PC pc ON p.model=pc.model
WHERE p.type = 'PC' AND 
pc.speed >= 450

-----------------9---------------

SELECT model, price
FROM printer
WHERE price = (WHERE max(price) FROM printer)

-----------------10---------------

SELECT AVG(speed) AS Avg_speed
FROM PC

-----------------11---------------

SELECT AVG(speed) AS Avg_speed
FROM laptop
where price > 1000

-----------------12---------------

SELECT AVG(speed) AS Avg_speed 
FROM PC pc
JOIN Product pr ON pc.model = pr.model AND
pr.maker LIKE 'A'

-----------------13---------------

SELECT DISTINCT cl.class, s.name, cl.country 
FROM Classes cl, Ships s 
WHERE cl.class =  s.class AND 
cl.numGuns > '10'

-----------------14---------------

SELECT hd
FROM PC pc
GROUP BY hd
HAVING Count(Hd)>1

-----------------15---------------

SELECT pc.model, pc1.model, pc.speed, pc.ram 
FROM PC pc JOIN (SELECT speed, ram
FROM PC GROUP BY speed, ram
HAVING COUNT(speed) >= 2 AND 
COUNT(ram) >= 2 ) s ON 
pc.speed = s.speed AND 
pc.ram = s.ram JOIN PC pc1 ON pc1.speed = s.speed AND pc1.ram = s.ram AND pc1.model < pc.model

-----------------16---------------

SELECT DISTINCT 'Laptop' AS type, model, speed 
FROM Laptop
WHERE speed < (SELECT MIN(speed) FROM PC)

-----------------17---------------

SELECT c.maker, a.price AS price
FROM (SELECT MIN(price) price 
FROM Printer WHERE Color ='y') a INNER JOIN 
Printer p ON a.priceA = p.price INNER JOIN 
Product pr ON p.model = pr.model;

-----------------18---------------

SELECT pr.maker AS Maker, AVG(screen) AS Avg_screen 
FROM Product pr, Laptop lt
WHERE pr.model = lt.model 
GROUP BY pr.maker

-----------------19---------------

SELECT maker , COUNT(model) as Count_Model 
FROM product
WHERE type = 'pc' 
GROUP BY maker 
HAVING COUNT(model) >= 3

-----------------20---------------

Select Product.maker, MAX(PC.price) as Max_price 
FROM PC, PRODUCT 
Where Product.model = PC.model and 
Product.type = 'PC' 
GROUP BY Product.maker

-----------------21---------------