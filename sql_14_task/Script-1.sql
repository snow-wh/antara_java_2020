SELECT pc.speed, avg(pc.price)
FROM pc
GROUP BY pc.speed
HAVING (pc.speed>600)

-----------------22---------------


SELECT DISTINCT maker
FROM product
WHERE model IN ( SELECT model FROM PC WHERE speed >= 750)
OR model IN (SELECT model FROM Laptop WHERE speed >= 750);

-----------------23---------------


WITH Max_price AS
(SELECT model, price
FROM PC
UNION
SELECT model, price
FROM Laptop
UNION
SELECT model, price
FROM Printer)
SELECT model from Max_price
WHERE price=(SELECT MAX(price) from Max_price)

-----------------24---------------


SELECT DISTINCT maker
FROM product, pc
WHERE product.model = pc.model 
AND pc.ram = (SELECT min(ram) FROM pc)
AND pc.speed = (SELECT max(speed) FROM pc)
WHERE ram = (SELECT min(ram) FROM pc)
AND maker IN (SELECT maker FROM product pr WHERE pr.type LIKE 'printer' )

-----------------25---------------


SELECT AVG(price) 
FROM (SELECT price FROM pc WHERE model IN
(SELECT model FROM product pr WHERE pr.maker LIKE 'A' AND pr.type LIKE 'PC')
UNION ALL
SELECT price FROM laptop WHERE model IN
(SELECT model FROM product pr WHERE pr.maker LIKE 'A' AND pr.type LIKE'Laptop') as prod

-----------------26---------------


SELECT pr.maker, AVG(PC.hd) 
FROM (SELECT maker, model FROM Product pr WHERE pr.type LIKE 'PC') as pr, PC pc 
WHERE pr.model=pc.model
AND pr.maker in (SELECT DISTINCT maker FROM Product pr WHERE pr.type LIKE 'Printer')
GROUP BY pr.maker

-----------------27---------------


SELECT COUNT(pr.Maker) AS qty
FROM Product pr
GROUP BY pr.maker
HAVING COUNT(pr.model)=1

-----------------28---------------


SELECT
CASE
WHEN i.point IS NOT NULL
THEN i.point
ELSE o.point
END point,
CASE
WHEN i.date IS NOT NULL
THEN i.date
ELSE o.date
END DATE, I.inc, O.out
FROM Income_o i FULL JOIN Outcome_o o ON i.point = o.point 
AND i.date = o.date

-----------------29---------------


SELECT
CASE
WHEN i.point is NULL 
THEN o.point
ELSE i.point
END point  ,
CASE
WHEN i.date is NULL 
THEN o.date
ELSE i.date
END date ,o.Outcome,i.Income   
FROM (SELECT point, date, SUM (out) as Outcome FROM Outcome
  GROUP BY point, date) o
FULL JOIN
(SELECT point, date, SUM (inc) as Income FROM Income
  GROUP BY point, date) i
ON
o.point = i.point and o.date = i.date
     
     
-----------------30---------------

     
     
     
     
     
     
     
     
     
     
     
     
     
     
     