SELECT ad.*
FROM AIRCRAFTS_DATA ad
JOIN AIRCRAFTS_DATA_ARCHIVE ada 
ON ad."RANGE" = ada."RANGE" ;

-----------------1---------------

SELECT ad.*
FROM AIRCRAFTS_DATA ad
WHERE ad."RANGE" > ALL ( SELECT ada."RANGE" FROM AIRCRAFTS_DATA_ARCHIVE ada);
	
-----------------2---------------

SELECT ad.*
FROM AIRCRAFTS_DATA ad
WHERE ad."RANGE" > ANY (SELECT ada."RANGE" FROM AIRCRAFTS_DATA_ARCHIVE ada);
	
-----------------3---------------

SELECT ad.*
FROM AIRCRAFTS_DATA ad
WHERE ad."RANGE" = (SELECT MIN(ada."RANGE") FROM AIRCRAFTS_DATA_ARCHIVE ada);
	
-----------------4---------------
	

SELECT ad.*
FROM AIRCRAFTS_DATA ad
INNER JOIN AIRCRAFTS_DATA_ARCHIVE ada 
ON ad.MODEL = ada.MODEL;

-----------------5---------------


SELECT ad.*, ada."CONSTRUCTOR"
FROM AIRCRAFTS_DATA ad
FULL JOIN AIRCRAFTS_DATA_ARCHIVE ada 
ON ad.MODEL = ada.MODEL;

-----------------6---------------

SELECT ad.CITY, COUNT(ad.AIRPORT_NAME)
FROM AIRPORTS_DATA ad
GROUP BY AD.CITY
HAVING COUNT(AIRPORT_NAME)>1;

-----------------7---------------

SELECT ad.*
FROM AIRCRAFTS_DATA ad
JOIN (SELECT f.AIRCRAFT_CODE 
		FROM FLIGHTS f 
		WHERE f.DATE_DEPARTURE != '01.09.2017' 
		GROUP BY
		f.AIRCRAFT_CODE ) f 
ON f.AIRCRAFT_CODE = ad.AIRCRAFT_CODE;

-----------------8---------------

SELECT ad.*
FROM AIRCRAFTS_DATA ad
JOIN ( SELECT f.AIRCRAFT_CODE
		FROM FLIGHTS f
		WHERE f.DATE_DEPARTURE BETWEEN '01.09.2017' AND '02.09.2017'
		GROUP BY f.AIRCRAFT_CODE) f 
ON f.AIRCRAFT_CODE = ad.AIRCRAFT_CODE
JOIN ( SELECT s.AIRCRAFT_CODE
		FROM SEATS s
		GROUP BY s.AIRCRAFT_CODE
		HAVING COUNT(s.SEAT_NO)>150) s 
ON s.AIRCRAFT_CODE = f.AIRCRAFT_CODE ;

-----------------9---------------

SELECT f.*
FROM FLIGHTS f
JOIN AIRCRAFTS_DATA ad 
ON f.AIRCRAFT_CODE = ad.AIRCRAFT_CODE
WHERE f.DATE_DEPARTURE BETWEEN '01.09.2017' AND '02.09.2017'
AND ad."RANGE" >= 8000;

-----------------10---------------

SELECT t.*
FROM TICKET_FLIGHTS t
WHERE t.AMOUNT > ALL ( SELECT tf.AMOUNT
						FROM TICKET_FLIGHTS tf
						WHERE tf.TICKET_NO = 0005432661915 )
ORDER BY t.AMOUNT ;

-----------------11---------------

SELECT t.*
FROM TICKET_FLIGHTS t
WHERE t.AMOUNT = ANY ( SELECT tf.AMOUNT
						FROM TICKET_FLIGHTS tf
						WHERE tf.TICKET_NO = 0005432661915 )
ORDER BY
t.AMOUNT ;

-----------------12---------------
