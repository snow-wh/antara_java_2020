package test;

import framework.BaseTest;
import org.junit.Test;

public class AvitoTest extends BaseTest {

    @Test
    public void selectTest(){

        AvitoPage page = new AvitoPage(getDriver());
        page.selectCategory("Оргтехника и расходники");
        page.inputTextInSearchField("принтер");
        page.searchForRegion("Владивосток");
        page.choiceDeliveryCheckbox();
        page.sortSearch("Дороже");
        page.outNameAndPrice();

    }

}
