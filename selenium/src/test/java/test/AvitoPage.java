package test;

import framework.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;

public class AvitoPage extends BasePage {

    public AvitoPage(WebDriver driver) {
        super(driver);
        open();
    }

    @Override
    protected void open() {
        driver.get("https://www.avito.ru/");
    }


    @FindBy(xpath = "//select[@id='category']")
    private WebElement category;
    @FindBy(xpath = "//input[@id='search']")
    private WebElement searchField;
    @FindBy(xpath = ("//div[contains(@data-marker,'region')]"))
    private WebElement searchRegion;
    @FindBy(xpath = ("//input[contains(@data-marker,'region/input')]"))
    private WebElement searchRegionField;
    @FindBy(xpath =("//li[@data-marker='suggest(0)']"))
    private WebElement clickSearchRegion;
    @FindBy(xpath = ("//button[contains(@data-marker,'save-button')]"))
    private WebElement searchRegionButton;
    @FindBy(xpath = ("//span[contains(@data-marker,'delivery-filter')]"))
    private WebElement checkboxDelivery;
    @FindBy(xpath = ("//div[contains(@class,'sort-select')]/select[contains(@class,'select')]"))
    private WebElement sortSearch;
    @FindBy (xpath = ("//a[@data-marker='item-title']/span[@itemprop='name']"))
    private List<WebElement> listElementName;
    @FindBy (xpath = ("//span[@data-marker='item-price']/span[contains(@class,'price')]"))
    private List<WebElement> listElementPrice;

    public void selectCategory(String str){
        select(category).selectByVisibleText(str);
    }

    public void inputTextInSearchField(String str){
        searchField.sendKeys(str);
    }

    public void searchForRegion(String str){
        searchRegion.click();
        searchRegionField.sendKeys(str);
        try {
            Thread.sleep(1_000);
        } catch (InterruptedException e){
            e.printStackTrace();
        }

        clickSearchRegion.click();
        searchRegionButton.click();
    }

    public void choiceDeliveryCheckbox(){
        if (!checkboxDelivery.isSelected()){
            checkboxDelivery.click();
        }
    }

    public void sortSearch(String str){
        select(sortSearch).selectByVisibleText(str);
    }

    public void outNameAndPrice(){

        for (int i = 0; i < 3 ; i++) {
            System.out.println(listElementName.get(i).getText());
            System.out.println(listElementPrice.get(i).getText());
        }

    }

}
