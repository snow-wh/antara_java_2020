package framework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {
    private static final int TIMEOUT = 15;
    private static final int POLLING = 100;

    protected WebDriver driver;
    private WebDriverWait wait;
    private Select select;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, TIMEOUT, POLLING);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, TIMEOUT),this);
    }
    protected abstract void open();

    protected void waitForElementToAppear(By locator) {
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    protected Select select(WebElement element){
            select = new Select(element);
        return select;
    }

}
