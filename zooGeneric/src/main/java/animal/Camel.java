package animal;

public class Camel extends Herbivorous {


    public Camel(int weight, String name) {
        super(weight, name);
    }

    @Override
    public String voice() {
        return "мумуму";
    }


}
