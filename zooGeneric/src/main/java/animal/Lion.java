package animal;

public class Lion extends Carnivorous {


    public Lion(String name, int weight) {
        super(name,weight);

    }

    @Override
    public String voice() {
        return "rrrrr";
    }


}
