package animal;

import food.AnimalOrigin;
import food.Food;


public class Duck extends Herbivorous {


    private int weight;

    public Duck(int weight, String name) {
        super(weight, name);
        this.weight = weight;
    }

    @Override
    protected String voice() {
        if (getKill() == false)
            return "крякря";
        else
            return "убили";
    }



    public Food kill() throws Exception {
        if (getKill() == false) {
            setKill(true);
            return new AnimalOrigin(1, 308 * weight);
        } else {
            throw new Exception();
        }
    }

}
