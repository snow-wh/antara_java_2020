package animal;


import FeedAll.FeedAll;
import food.Food;

public abstract class Animal implements FeedAll {

    private boolean kill = false;
    private String name;

    protected Animal(String name){
        this.name = name;
    }


    protected abstract String voice();

    public String name(){
        return name;
    }


    public abstract int weight();

    protected abstract int amountOfFood(Food food);

    protected boolean getKill(){
        return kill;
    }
    protected void setKill(boolean kill){
        this.kill=kill;
    }

    public void eat(Food food) {
        int returnAmount = amountOfFood(food);

        switch (returnAmount) {
            case -1:
                System.out.println("Принесите " + name() + "'y мяса");
                break;
            case 0:
                System.out.println(name() + "'a пока нельзя кормить");
                break;
            case 1:
                System.out.println(name() + " наелся, повторите через 5 часов");
                break;
            default:
                if (returnAmount > 0)
                    System.out.println(name() + "'y не хватило " + returnAmount + " кКалорий, добавьте порцию");
                else
                    System.out.println(name() + "'у многовато, уменьшите порцию на  " + Math.abs(returnAmount) + " кКалорий");

        }
    }

    @Override
    public void feedAll(Food food){
        eat(food);
    }


}

