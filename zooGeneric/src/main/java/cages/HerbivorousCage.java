package cages;

import animal.Herbivorous;

import food.BirdFood;


public class HerbivorousCage<T extends Herbivorous> extends Cage {

    public HerbivorousCage(int length, int width) {
        super(length, width);
    }


    public void moveIn(T animal) {

        if (death(animal)) {
            if (presence(animal)) {
                try {
                    in(animal);
                    System.out.println(animal.name() + " живет теперь тут");
                } catch (Exception e) {
                    System.out.println("Животному недостаточно места");
                }

            } else {
                System.out.println("Это животное уже в клетке");
            }
        } else {
            System.out.println("Это животное мертво");
        }


    }

    public void moveOut(T animal) {

        try {
            out(animal);
            System.out.println(animal.name() + " выселен из клетки");
        } catch (Exception e) {
            System.out.println("Животного с таким именем нет в клетке");
        }

    }

    public void feedAll(){

        feedAnimal(new BirdFood(1));
    }

}
