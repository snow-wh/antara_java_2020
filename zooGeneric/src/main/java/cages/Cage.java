package cages;



import FeedAll.FeedAll;
import animal.Animal;
import food.Food;

import java.util.HashMap;
import java.util.Map;

public class Cage{

    private int length;
    private int width;
    private FeedAll feedAll;

    private final int BIG_ANIMAL = 100;
    private final int MID_ANIMAL = 50;
    private final int SMALL_ANIMAL = 10;
    private final int OVER_SMALL_ANIMAL = 3;

    private Map<String, Animal> neighbours = new HashMap<String, Animal>();



    public Cage(int length, int width) {
        this.length = length;
        this.width = width;
    }

    /**
     * Cage.in
     * if input mass != 8 standard => Exception
     */
    protected Map in(Animal animal) throws Exception {

        int allWeight = 0;
        int count = 1;
        if (!neighbours.isEmpty()) {
            for (Animal animals : neighbours.values()) {
                allWeight += animals.weight();
                count += 1;
            }
        }
        if ((allWeight + animal.weight()) >= (count * 700)) {
            if (length * width >= count * BIG_ANIMAL) {
                neighbours.put(animal.name(), animal);
                return neighbours;
            } else {
                return null;
            }
        } else if (allWeight + animal.weight() >= count * 400 && count * 700 < allWeight + animal.weight()) {
            if (length * width >= count * MID_ANIMAL) {
                neighbours.put(animal.name(), animal);
                return neighbours;
            } else {
                return null;
            }
        } else if (allWeight + animal.weight() >= count * 150 && count * 400 < allWeight + animal.weight()) {
            if (length * width >= count * SMALL_ANIMAL) {
                neighbours.put(animal.name(), animal);
                return neighbours;
            } else {
                return null;
            }
        } else if (allWeight + animal.weight() >= count * 5 && count * 150 < allWeight + animal.weight()) {
            if (length * width >= count * OVER_SMALL_ANIMAL) {
                neighbours.put(animal.name(), animal);
                return neighbours;
            } else {
                return null;
            }
        }

        throw new Exception();

    }

    protected boolean death(Animal animal) {
        return animal.weight() != 0;
    }

    protected boolean presence(Animal animal) {
        return !neighbours.containsKey(animal.name());
    }

    protected Map out(Animal animal) throws Exception {

        if (neighbours.containsKey(animal.name())) {
            neighbours.remove(animal.name());
            return neighbours;
        } else {
            throw new Exception();
        }
    }

    protected void feedAnimal(Food food){

        for (Animal animals: neighbours.values()) {
            feedAll = animals;
            feedAll.feedAll(food);
        }
    }


}
