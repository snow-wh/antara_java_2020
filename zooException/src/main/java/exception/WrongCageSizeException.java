package exception;

public class WrongCageSizeException extends Throwable {

    public WrongCageSizeException(){
        super("Неподходящий размер клетки для этого животного");
    }
}
