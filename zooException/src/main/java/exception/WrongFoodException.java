package exception;

public class WrongFoodException extends Throwable {

    public WrongFoodException(){
        super("Неподходящая еда для данного типа животного");
    }

}
