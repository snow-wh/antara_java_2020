package food;

public class BirdFood extends HerbivorousOrigin{
    private final int calories = 100;

    public BirdFood(){
        setCalories(calories);
    }

    public BirdFood(int countFud ){
        super(countFud);
        setCalories(calories);
    }

}
