package cage;

import exception.WrongCageSizeException;
import exception.WrongCageContainException;

import animals.Carnivorous;
import food.Chicken;




public class CarnivorousCage <T extends Carnivorous>  extends Cage{


    public CarnivorousCage(int length, int width) {
        super(length, width);
    }


    public void moveIn(T animal) throws WrongCageSizeException {

        if (!animal.getKill()) {
            if (containsAnimal(animal)) {
                    in(animal);
                    System.out.println(animal.name() + " живет теперь тут");

            } else {
                System.out.println("Это животное уже в клетке");
            }
        } else {
            System.out.println("Это животное мертво");
        }
    }


    public void moveOut(T animal) throws WrongCageContainException {


            out(animal);
            System.out.println(animal.name() + " выселен из клетки");


    }

    public void feedAll(){

        feedAnimal(new Chicken(1));
    }

}
