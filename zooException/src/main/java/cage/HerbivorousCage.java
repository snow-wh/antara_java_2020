package cage;

import exception.WrongCageSizeException;
import exception.WrongCageContainException;

import animals.Herbivorous;

import food.Chicken;



public class HerbivorousCage<T extends Herbivorous> extends Cage  {


    public HerbivorousCage(int length, int width) {
        super(length, width);
    }


    public void moveIn(T animal) {

        if (!animal.getKill()) {
            if (containsAnimal(animal)) {
                try {
                    in(animal);
                    System.out.println(animal.name() + " живет теперь тут");
                } catch (WrongCageSizeException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Это животное уже в клетке");
            }
        } else {
            System.out.println("Это животное мертво");
        }
    }

    public void moveOut(T animal) {

        try {
            out(animal);
            System.out.println(animal.name() + " выселен из клетки");
        } catch (WrongCageContainException e) {
            e.printStackTrace();
        }

    }
    public void feedAll() {
        feedAnimal(new Chicken(1));
   }
}
