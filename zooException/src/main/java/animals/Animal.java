package animals;

import FeedAll.FeedAll;
import exception.WrongFoodException;
import food.Food;
import sizeEnum.CageSize;



public abstract class Animal implements FeedAll {

    protected Animal(String name){
        this.name =name;
    }

    protected String name;
    private boolean kill = false;

    public String name(){
        return name;
    }

    public abstract CageSize getCageSize();

    protected abstract String voice();

    protected abstract int amountOfFood(Food food) throws WrongFoodException;

    public boolean getKill(){
        return kill;
    }

    protected void setKill(boolean kill){
        this.kill=kill;
    }

    public void eat(Food food) throws WrongFoodException {
        int returnAmount = 0;
            returnAmount = amountOfFood(food);
            switch (returnAmount) {
                case 0:
                    System.out.println(name() + "'a пока нельзя кормить");
                    break;
                case 1:
                    System.out.println(name() + " наелся, повторите через 5 часов");
                    break;
                default:
                    if (returnAmount > 0){
                        System.out.println(name() + "'y не хватило " + returnAmount + " кКалорий, добавьте порцию");
                    }
                    else{
                        System.out.println(name() + "'у многовато, уменьшите порцию на  " + Math.abs(returnAmount) + " кКалорий");
                    }

            }
    }

    @Override
    public void feedAll(Food food){
        try {
            eat(food);
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }
    }

}
