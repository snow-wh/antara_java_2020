
abstract class Human {


    public int year;
    public String name;


    public abstract String profession();

    public abstract int experience();

    public Human() {
    }

    public Human(String name, int year) {
        this.name = name;
        this.year = year;
    }

    public int getYear() {
        return year;
    }


    protected void setYear(int year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

}

