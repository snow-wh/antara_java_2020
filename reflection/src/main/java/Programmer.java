public class Programmer extends Human implements Task {

    private int salary = 30000;
    public String profession = "Programmer";
    private String task = "Make program";
    private int experience;

    public Programmer() {
    }

    public Programmer(String name, int year, int experience) {
        super(name, year);
        this.experience = experience;
    }


    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    @Override
    public String profession() {
        return profession;
    }

    @Override
    public int experience() {
        return experience;
    }


    public String task() {
        return task;
    }
}
