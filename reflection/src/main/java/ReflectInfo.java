import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ReflectInfo<T> {

    public Class reflectInfo(T obj){

        Class clazz = obj.getClass();
        System.out.println("Class Name: " + clazz.getSimpleName());
        Class superClazz = clazz.getSuperclass();
        System.out.println("SuperClass name: " + superClazz.getSimpleName());

        Constructor<?>[] constructors = clazz.getConstructors();
        System.out.println("--- constructors: ");
        System.out.println(Arrays.toString(constructors));

        System.out.println("--- public fields: ");
        Field[] fieldsPublic = clazz.getFields();
        System.out.println(Arrays.toString(fieldsPublic));

        System.out.println("--- all fields: ");
        Field[] fieldsAll = clazz.getDeclaredFields();
        Arrays.stream(fieldsAll).forEach(field -> System.out.println(field.getName()));

        System.out.println("--- public method: ");
        Method[] publicMethod = clazz.getMethods();
        Arrays.stream(publicMethod).forEach(field -> System.out.println(field.getName()));

        System.out.println("--- public super class method: ");
        Method[] superClassPublicMethod = superClazz.getMethods();
        Arrays.stream(superClassPublicMethod).forEach(field -> System.out.println(field.getName()));

        System.out.println("--- all method: ");
        Method[] AllMethods = clazz.getDeclaredMethods();
        Arrays.stream(AllMethods).forEach(field -> System.out.println(field.getName()));
        System.out.println("--- all super class method: ");
        Method[] superClassAllMethods= superClazz.getMethods();
        Arrays.stream(superClassAllMethods).forEach(field -> System.out.println(field.getName()));

        return clazz;
    }

}
