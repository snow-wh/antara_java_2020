import java.lang.reflect.*;
import java.util.Arrays;

public class main {


    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {

        /**
         * 1 вытащить поля
         * 2 вытащить конструкотры
         * 3 сделать поле паблик
         * 4 заменить значение и вывести
         * */


        ReflectInfo<Programmer> programmer = new ReflectInfo<>();
        Class clazz = programmer.reflectInfo(new Programmer());


        Constructor<?> constructor = clazz.getConstructor(String.class, int.class, int.class);
        Object obj = constructor.newInstance("Mike", 21, 2);

        Method profession = clazz.getDeclaredMethod("profession");
        profession.setAccessible(true);
        profession.invoke(obj);

        Method task = clazz.getDeclaredMethod("task");
        task.setAccessible(true);
        task.invoke(obj);

        Method experience = clazz.getDeclaredMethod("experience");
        experience.setAccessible(true);
        experience.invoke(obj);

        System.out.println("--- изменение приватного поля:");
        Field field = clazz.getDeclaredField("salary");
        field.setAccessible(true);
        field.set(obj, 100);
        System.out.println("новое значение: " + field.get(obj));


    }


}
