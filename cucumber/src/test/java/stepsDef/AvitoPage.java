package stepsDef;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


import java.util.List;

public class AvitoPage extends BasePage  {

    public AvitoPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected void open() {
        driver.get("https://www.avito.ru/");
    }

    @FindBy(xpath = "//select[@id='category']")
    private WebElement category;
    @FindBy(xpath = "//input[@id='search']")
    private WebElement searchField;
    @FindBy(xpath = ("//div[contains(@data-marker,'region')]"))
    private WebElement searchRegion;
    @FindBy(xpath = ("//input[contains(@data-marker,'region/input')]"))
    private WebElement searchRegionField;
    @FindBy(xpath =("//li[@data-marker='suggest(0)']"))
    private WebElement clickSearchRegion;
    @FindBy(xpath = ("//button[contains(@data-marker,'save-button')]"))
    private WebElement searchRegionButton;
    @FindBy(xpath = ("//span[text()='только с фото']"))
    private WebElement checkboxWithImages;
    @FindBy(xpath = ("//div[@data-marker='page-title']"))
    private WebElement titleSearchText;
    @FindBy(xpath = ("//div[contains(@class,'sort-select')]/select[contains(@class,'select')]"))
    private WebElement sortSearch;
    @FindBy (xpath = ("//a[@data-marker='item-title']/span[@itemprop='name']"))
    private List<WebElement> listElementName;
    @FindBy (xpath = ("//span[@data-marker='item-price']/span[contains(@class,'price')]"))
    private List<WebElement> listElementPrice;

    public void selectCategory(String str){
        select(category).selectByVisibleText(str);
    }

    public void inputTextInSearchField(String str){
        searchField.sendKeys(str);
    }

    public void clickOnDropRegion(){
        searchRegion.click();
    }

    public void searchForRegion(String str){
        searchRegionField.sendKeys(str);
        try {
            Thread.sleep(1_000);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        clickSearchRegion.click();
    }

    public void clickSearchRegionButton(){
        searchRegionButton.click();
    }

    public boolean searchTitle(String str){
        return titleSearchText.getText().contains(str);
    }

    public void choiceWithImagesCheckbox(){
        if (!checkboxWithImages.isSelected()){
            checkboxWithImages.click();
        }
        try {
            Thread.sleep(1_000);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public void sortSearch(String str){
        select(sortSearch).selectByVisibleText(str);
    }

    public void outNameAndPrice(int num){

        for (int i = 0; i < num ; i++) {
            System.out.println(listElementName.get(i).getText());
            System.out.println(listElementPrice.get(i).getText());
        }

    }

}
