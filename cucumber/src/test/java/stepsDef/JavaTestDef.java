package stepsDef;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;


public class JavaTestDef {


    private WebDriver driver;
    private AvitoPage page;

    @Given("Opened avito")
    public void openedAvito() {
        System.setProperty("webdriver.gecko.driver","src\\test\\java\\resources\\geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        page = new AvitoPage(driver);
        page.open();
    }

    @Then("In drop-down list category choose {string}")
    public void inDropDownListCategoryChoose(String str) {
        page.selectCategory(str);
    }

    @Then("Write in search field {string}")
    public void writeInSearchField(String str) {
        page.inputTextInSearchField(str);
    }

    @Then("Click on drop-down list regions")
    public void clickOnDropDownListRegions() {
        page.clickOnDropRegion();
    }

    @And("Write in field region {string}")
    public void writeInFieldRegion(String str) {
        page.searchForRegion(str);
    }

    @And("Click on search button")
    public void clickOnSearchButton() {
        page.clickSearchRegionButton();
    }

    @Then("Opened page with {string} result")
    public void openedPageWithResult(String str) {
        assertTrue(page.searchTitle(str));
    }

    @Then("Activate checkbox Только с фотографией")
    public void activateCheckbox() {
        page.choiceWithImagesCheckbox();
    }

    @Then("In drop-down list sort choose {string}")
    public void inDropDownListSortChoose(String str) {
        page.sortSearch(str);
    }

    @And("Out in console value назвние and цена {int} first item")
    public void outInConsoleValueAnyFirstItem(int num) {
        page.outNameAndPrice(num);
    }

    @Then("Close browser")
    public void closeBrowser() {
        if(null != driver) {
            driver.close();
        }
    }
}
