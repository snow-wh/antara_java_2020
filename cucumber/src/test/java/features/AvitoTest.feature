@1
Feature: search on avito
  Scenario: Let's find the most expensive printers on avito
    Given Opened avito
    Then In drop-down list category choose "Оргтехника и расходники"
    Then Write in search field "принтер"
    Then Click on drop-down list regions
    And Write in field region "Владивосток"
    And Click on search button
    Then Opened page with "принтер" result
    Then Activate checkbox Только с фотографией
    Then In drop-down list sort choose "Дороже"
    And Out in console value назвние and цена 3 first item
    Then Close browser


