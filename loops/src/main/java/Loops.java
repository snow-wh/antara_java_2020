
import java.util.Scanner;

public class Loops {
    public static int[] sortMas(int[] inputMas) {
        for (int i = 0; i < inputMas.length; i++) {
            for (int j = 0; j < inputMas.length - 1; j++) {
                if (inputMas[j] > inputMas[j + 1]) {
                    inputMas[j] = inputMas[j] + inputMas[j + 1];
                    inputMas[j + 1] = inputMas[j] - inputMas[j + 1];
                    inputMas[j] = inputMas[j] - inputMas[j + 1];
                }
            }
        }
        return inputMas;
    }

    public static boolean lengthWord(String sentence, int lengthWord) {
        String[] elementMas = sentence.split(" ");
        int[] lengthElement = new int[elementMas.length];
        for (int i = 0; i < elementMas.length; i++) {
            lengthElement[i] = elementMas[i].length();
        }
        lengthElement = sortMas(lengthElement);
        int firstIndex = 0;
        int lastIndex = elementMas.length - 1;
        while (firstIndex <= lastIndex) {
            int midIndex = (firstIndex + lastIndex) / 2;
            if (lengthElement[midIndex] == lengthWord) {
                return true;
            } else if (lengthElement[midIndex] < lengthWord) {
                firstIndex = midIndex + 1;
            } else {
                lastIndex = midIndex - 1;
            }
        }
        return false;
    }

    public static void run() {
        Scanner in = new Scanner(System.in);
        String s;
        int l;
        char ch;
        do {
            System.out.println("Введите строку: ");
            in.next();
            s = in.nextLine();
            System.out.println("Введите длинну подстроки, которую нужно найти");
            l = in.nextInt();
            System.out.println(!lengthWord(s, l) ? "слова с такой длиной нет" : "слово с такой длиной есть");
            System.out.println("Введите любую клавишу для продолжения, для выхода q");
            ch = in.next().charAt(0);
        } while (ch != 'q');
    }

    public static void main(String[] args){
        run();
    }
}

